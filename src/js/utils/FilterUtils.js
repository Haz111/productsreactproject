var Filter = {

    filterImageContains: function(products, contains) {
        if (contains === null) return products;

        let filteredProducts = [];
        products.forEach(p => {
            if (p.image !== null && contains === true)
                filteredProducts.push(p);
            else if (p.image === null && contains === false)
                filteredProducts.push(p);
        });

        return filteredProducts;
    },

    filterRatingMin: function (products, min) {
        if (min === null) return products;

        let filteredProducts = [];
        products.forEach(p => {
            if (p.rating >= min)
                filteredProducts.push(p);
        });

        return filteredProducts;
    }
};

module.exports = Filter;