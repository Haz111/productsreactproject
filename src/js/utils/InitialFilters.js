var InitialFilters = {

    getInitialFilters() {
        return {
            orientation: {
                type: "multiple",
                options: [
                    {
                        name: "Horizontal",
                        checked: false
                    },
                    {
                        name: "Vertical",
                        checked: false
                    }
                ]
            },
            sizeFilter: {
                type: "multiple",
                options: [
                    {
                        name: "90x50mm",
                        checked: false
                    },
                    {
                        name: "85x55mm",
                        checked: false
                    }
                ]
            },
            customerRating: {
                type: "single",
                options: [4, 3, 2, 1],
                checked: null
            },
            favorite: {
                type: "multiple_custom",
                options: [
                    {
                        name: "Only favorites",
                        checked: false
                    }
                ]
            },
            industry: {
                type: "multiple",
                options: [
                    {
                        name: "Automotive",
                        checked: false
                    },
                    {
                        name: "Fashion",
                        checked: false
                    },
                    {
                        name: "Law",
                        checked: false
                    },
                    {
                        name: "IT",
                        checked: false
                    },
                    {
                        name: "Sport",
                        checked: false
                    },
                    {
                        name: "Art",
                        checked: false
                    }
                ]
            },
            color: {
                type: "multiple_custom",
                options: [
                    {
                        name: "#111111",
                        checked: false
                    },
                    {
                        name: "#111111",
                        checked: false
                    },
                    {
                        name: "#111111",
                        checked: false
                    },
                    {
                        name: "#111111",
                        checked: false
                    },
                    {
                        name: "#111111",
                        checked: false
                    },
                    {
                        name: "#111111",
                        checked: false
                    }
                ]
            },
            style: {
                type: "multiple",
                options: [
                    {
                        name: "Abstraction",
                        checked: false
                    },
                    {
                        name: "Geometry",
                        checked: false
                    },
                    {
                        name: "Men",
                        checked: false
                    },
                    {
                        name: "Women",
                        checked: false
                    },
                    {
                        name: "Children",
                        checked: false
                    },
                    {
                        name: "Music",
                        checked: false
                    },
                    {
                        name: "Vintage",
                        checked: false
                    },
                    {
                        name: "Flower",
                        checked: false
                    }
                ]
            }
        }
    }

};

module.exports = InitialFilters;
