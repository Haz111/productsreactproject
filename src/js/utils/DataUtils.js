var FakeData = {

    getFakeProducts: function (count) {
        let products = [];
        for (let i=0; i<count; i++) {
            products.push(this.getFakeProduct())
        }

        return products;
    },

    getFakeProduct: function() {
        let names = ["Returns", "Random", "Number", "Between", "Inclusive",  "And", "Exclusive"];
        let sizes = ["90x50mm", "85x55mm"];

        let product = {};
        product.name = names[Math.floor(Math.random() * names.length)];
        product.image = "/images/notes2.png";
        product.rating = Math.floor(Math.random() * 6);
        product.favourite = Math.random() < 0.3;
        product.size = sizes[Math.floor(Math.random() * sizes.length)];

        return product;
    }
};

module.exports = FakeData;