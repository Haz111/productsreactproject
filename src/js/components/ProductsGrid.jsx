import React from "react";
import ProductCard from "./ProductCard.jsx";

export default class ProductsGrid extends React.Component {

    render() {
        let cssClasses = "ProductsGrid";
        return (
            <div className={cssClasses}>
                {this.props.products.map((product, i) =>
                    <ProductCard
                        key={i}
                        index={i}
                        product={product}
                        toggleFavourite={this.props.toggleFavourite}
                    />
                )}
            </div>
        );
    }
}
