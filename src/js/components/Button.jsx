import React from "react";

export default class Button extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let cssClasses = "Button";
        return (
            <button className={cssClasses} onClick={() => {this.props.onClick()}}>{this.props.label}</button>
        );
    }
}

