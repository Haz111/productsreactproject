import React from "react";
import StarsRating from "./StarsRating.jsx";
import FavouriteButton from "./FavouriteButton.jsx";

export default class ProductCard extends React.Component {

    constructor(props){
        super(props);
        this.toggleFavourite = this.toggleFavourite.bind(this);
    }

    toggleFavourite() {
        this.props.toggleFavourite(this.props.index);
    }

    render() {
        let cssClasses = "ProductCard";
        return (
            <div className={cssClasses}>
                <img src={this.props.product.image}/>
                <div className="cardRatings">
                    <StarsRating rating={this.props.product.rating}/>
                    <FavouriteButton isFavourite={this.props.product.favourite} onClick={this.toggleFavourite}/>
                </div>

                <p className="productName">{this.props.product.name}</p>
                <p className="productSize">{this.props.product.size}</p>
            </div>
        );
    }
}
