import React from "react";

export default class GridTypeSelector extends React.Component {

    render() {
        let cssClasses = "GridTypeSelector";
        return (
            <div className={cssClasses}>
                <span>1</span>
                <span>2</span>
                <span>3</span>
            </div>
        );
    }
}
