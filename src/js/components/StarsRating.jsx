import React from "react";

export default class StarsRating extends React.Component {

    render() {
        let cssClasses = "StarsRating";
        let stars = [];
        let i;
        for (i = 0; i < 5; i++) {
            stars.push(i < this.props.rating ? <img key={i} src="/images/star_yellow.png"/> : <img key={i} src="/images/star_gray.png"/>);
        }

        return (
            <div className={cssClasses}>
                {stars}
            </div>
        );
    }
}
