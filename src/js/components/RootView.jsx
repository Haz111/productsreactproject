import React from "react";
import * as GenerateFakeData from "../utils/DataUtils";
import * as InitialFilters from "../utils/InitialFilters"
import FiltersPanel from "./FiltersPanel.jsx";
import * as Filter from "../utils/FilterUtils";
import ProductsPanel from "./ProductsPanel.jsx";

export default class RootView extends React.Component {

    constructor(props) {
        super(props);
        this.setSingleFilter = this.setSingleFilter.bind(this);
        this.setMultipleFilter = this.setMultipleFilter.bind(this);
        this.removeOneFilter = this.removeOneFilter.bind(this);
        this.toggleFavourite = this.toggleFavourite.bind(this);
        this.getClearedMultipleFilter = this.getClearedMultipleFilter.bind(this);
        this.getClearedSingleFilter = this.getClearedSingleFilter.bind(this);
        this.clearOneFilter = this.clearOneFilter.bind(this);
        this.clearAllFilters = this.clearAllFilters.bind(this);

        this.state = {
            products: GenerateFakeData.getFakeProducts(50),
            filters: InitialFilters.getInitialFilters()
        }
    }

    setSingleFilter(name, index) {
        let updatedFilter = Object.assign({}, this.state.filters);
        updatedFilter[name].checked = index;
        this.setState({
            filters: updatedFilter
        });
    }

    setMultipleFilter(name, index, value) {
        let updatedFilter = Object.assign({}, this.state.filters);
        updatedFilter[name].options[index].checked = value;
        this.setState({
            filters: updatedFilter
        });
    }

    removeOneFilter(name, index) {
        this.setMultipleFilter(name, index, false);
    }

    getClearedMultipleFilter(name) {
        let clearedFilter = Object.assign({}, this.state.filters[name]);
        for (var i = 0; i < clearedFilter.options.length; i++) {
            clearedFilter.options[i].checked = false;
        }
        return clearedFilter;
    }

    getClearedSingleFilter(name) {
        let clearedFilter = Object.assign({}, this.state.filters[name]);
        clearedFilter.checked = null;
        return clearedFilter;
    }

    clearOneFilter(name) {
        let updatedFilters = Object.assign({}, this.state.filters);

        if (updatedFilters.hasOwnProperty(name)) {
            if (updatedFilters[name].type === "multiple" || updatedFilters[name].type === "multiple_custom") {
                updatedFilters[name] = this.getClearedMultipleFilter(name);
            } else if (updatedFilters[name].type === "single") {
                updatedFilters[name] = this.getClearedSingleFilter(name);
            }
        }

        this.setState({
            filters: updatedFilters
        });
    }

    clearAllFilters() {
        let updatedFilters = Object.assign({}, this.state.filters);
        for (let filter in updatedFilters) {
            if (updatedFilters[filter].type === "multiple" || updatedFilters[filter].type === "multiple_custom") {
                updatedFilters[filter] = this.getClearedMultipleFilter(filter);
            } else if (updatedFilters[filter].type === "single") {
                updatedFilters[filter] = this.getClearedSingleFilter(filter);
            }
        }
        this.setState({
            filters: updatedFilters
        });
    }

    toggleFavourite(index) {
        let updatedProducts = [...this.state.products];
        updatedProducts[index].favourite = !updatedProducts[index].favourite;
        this.setState({
            products: updatedProducts
        });
    }

    render() {
        return (
            <div className="RootView">
                <FiltersPanel
                    filters={this.state.filters}
                    setSingleFilter={this.setSingleFilter}
                    setMultipleFilter={this.setMultipleFilter}
                    clearOneFilter={this.clearOneFilter}
                    clearAllFilters={this.clearAllFilters}
                />
                <ProductsPanel
                    products={this.state.products}
                    filters={this.state.filters}
                    removeFilter={this.removeOneFilter}
                    toggleFavourite={this.toggleFavourite}
                />
            </div>
        );
    }
}
