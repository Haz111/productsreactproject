import React from "react";
import StarRadioInput from "./StarRadioInput.jsx";

export default class CustomerRatingFilter extends React.Component {

    constructor(props) {
        super(props);
        this.radioInputChanged = this.radioInputChanged.bind(this);
        this.clearSelections = this.clearSelections.bind(this);
    }

    radioInputChanged(index) {
        this.props.setFilter("customerRating", index);
    }

    clearSelections() {
        this.props.setFilter("customerRating", null);
    }

    render() {
        let cssClasses = "CustomerRatingFilter filterBox";

        return (
            <div className={cssClasses}>
                <div className="filterHeading">
                    <div className="filterTitle">Customer Rating</div>
                    <div onClick={() => this.props.clearFilter("customerRating")}>x</div>
                </div>
                <span>At least</span>
                {this.props.filter.options.map((value, i) =>
                    <StarRadioInput
                        key={i}
                        index={i}
                        name={value + "CustomerRatingFilter"}
                        stars={value}
                        checked={this.props.filter.checked === i}
                        amount={10} // TODO: ustawic to dynamicznie
                        setFilter={this.radioInputChanged}
                    />
                )}
            </div>
        );
    }
}
