import React from "react";

export default class OrientationFilter extends React.Component {

    constructor(props) {
        super(props);
        this.horizontalCheckboxChanged = this.horizontalCheckboxChanged.bind(this);
        this.verticalCheckboxChanged = this.verticalCheckboxChanged.bind(this);
        this.updateValue = this.updateValue.bind(this);
    }

    updateValue(index) {
        let updatedValue = !this.props.filter.options[index].checked;
        this.props.setFilter("orientation", index, updatedValue);
    }

    horizontalCheckboxChanged() {
        this.updateValue(0);
    }

    verticalCheckboxChanged() {
        this.updateValue(1);
    }

    render() {
        let cssClasses = "OrientationFilter filterBox";
        return (
            <div className={cssClasses}>
                <div className="filterHeading">
                    <div className="filterTitle">Orientation</div>
                    <div onClick={() => this.props.clearFilter("orientation")}>x</div>
                </div>
                <div className="orientationCheckboxes">
                    <div className="horizontalCheckbox">
                        <input
                            type="checkbox"
                            id="orientationHorizontalCheckbox"
                            value="horizontal"
                            checked={this.props.filter.options[0].checked}
                            onChange={this.horizontalCheckboxChanged}
                        />
                        <label htmlFor="orientationHorizontalCheckbox">
                            horizontal
                        </label>
                    </div>
                    <div className="verticalCheckbox">
                        <input
                            type="checkbox"
                            id="orientationVerticalCheckbox"
                            value="vertical"
                            checked={this.props.filter.options[1].checked}
                            onChange={this.verticalCheckboxChanged}
                        />
                        <label htmlFor="orientationVerticalCheckbox">
                            vertical
                        </label>
                    </div>
                </div>
            </div>
        );
    }
}
