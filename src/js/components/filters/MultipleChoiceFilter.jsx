import React from "react";
import CheckboxInput from "./CheckboxInput.jsx";

export default class MultipleChoiceFilter extends React.Component {

    constructor(props){
        super(props);
        this.checkboxChanged = this.checkboxChanged.bind(this);
    }

    checkboxChanged(index, value) {
        this.props.setFilter(this.props.filterName, index, value);
    }

    render() {
        let cssClasses = "MultipleChoiceFilter filterBox";
        return (
            <div className={cssClasses}>
                <div className="filterHeading">
                    <div className="filterTitle">{this.props.filterTitle}</div>
                    <div onClick={() => this.props.clearFilter(this.props.filterName)}>x</div>
                </div>
                {this.props.filter.options.map((checkbox, i) =>
                    <CheckboxInput
                        key={i}
                        index={i}
                        name={checkbox.name}
                        checked={checkbox.checked}
                        amount={10} // TODO: ustawic to dynamicznie
                        setFilter={this.checkboxChanged}
                    />
                )}
            </div>


        );
    }
}
