import React from "react";
import StarsRating from "../StarsRating.jsx";

export default class StarRadioInput extends React.Component {

    constructor(props) {
        super(props);
        this.markRadioInput = this.markRadioInput.bind(this);
    }

    markRadioInput() {
        this.props.setFilter(this.props.index);
    }

    render() {
        let cssClasses = "StarRadioInput";
        let id = "StarRadioInput" + this.props.name.replace(/\s+/g, '');
        return (
            <div className={cssClasses}>
                <input
                    type="radio"
                    name="starRadioInput"
                    id={id}
                    value={this.props.stars}
                    checked={this.props.checked}
                    onChange={this.markRadioInput}
                />
                <label htmlFor={id}>
                    <StarsRating rating={this.props.stars}/>
                </label>
                {this.props.amount}
            </div>
        );
    }
}
