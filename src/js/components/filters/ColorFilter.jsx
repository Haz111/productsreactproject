import React from "react";

export default class ColorFilter extends React.Component {

    render() {
        let cssClasses = "ColorFilter filterBox";
        return (
            <div className={cssClasses}>
                <div className="filterHeading">
                    <div className="filterTitle">Color</div>
                    <div>x</div>
                </div>

            </div>
        );
    }
}
