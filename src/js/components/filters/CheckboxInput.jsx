import React from "react";

export default class CheckboxInput extends React.Component {

    constructor(props) {
        super(props);
        this.toggleCheckbox = this.toggleCheckbox.bind(this);
    }

    toggleCheckbox() {
        let updatedValue = !this.props.checked;
        this.props.setFilter(this.props.index, updatedValue)
    }

    render() {
        let cssClasses = "CheckboxInput";
        let id = "CheckboxInput" + this.props.name.replace(/\s+/g, '');
        return (
            <div className={cssClasses}>
                <input
                    value={this.props.name}
                    type="checkbox" id={id}
                    checked={this.props.checked}
                    onChange={this.toggleCheckbox}
                />
                <label htmlFor={id}>{this.props.name}</label>
                {this.props.amount}
            </div>
        );
    }
}
