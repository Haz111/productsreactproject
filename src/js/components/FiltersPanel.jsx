import React from "react";
import OrientationFilter from "./filters/OrientationFilter.jsx";
import CustomerRatingFilter from "./filters/CustomerRatingFilter.jsx";
import ColorFilter from "./filters/ColorFilter.jsx";
import MultipleChoiceFilter from "./filters/MultipleChoiceFilter.jsx";
import Button from "./Button.jsx";

export default class FiltersPanel extends React.Component {

    render() {
        let cssClasses = "FiltersPanel";

        return (
            <div className={cssClasses}>
                <p>Narrow results</p>
                <OrientationFilter
                    filter={this.props.filters.orientation}
                    setFilter={this.props.setMultipleFilter}
                    clearFilter={this.props.clearOneFilter}
                />
                <MultipleChoiceFilter
                    filterName="sizeFilter"
                    filterTitle="Size"
                    filter={this.props.filters.sizeFilter}
                    setFilter={this.props.setMultipleFilter}
                    clearFilter={this.props.clearOneFilter}
                />
                <CustomerRatingFilter
                    filter={this.props.filters.customerRating}
                    setFilter={this.props.setSingleFilter}
                    clearFilter={this.props.clearOneFilter}
                />
                <MultipleChoiceFilter
                    filterName="favorite"
                    filterTitle="Favorite"
                    filter={this.props.filters.favorite}
                    setFilter={this.props.setMultipleFilter}
                    clearFilter={this.props.clearOneFilter}
                />
                <MultipleChoiceFilter
                    filterName="industry"
                    filterTitle="Industry"
                    filter={this.props.filters.industry}
                    setFilter={this.props.setMultipleFilter}
                    clearFilter={this.props.clearOneFilter}
                />
                {/*<ColorFilter */}
                {/*setFilter={} />*/}
                <MultipleChoiceFilter
                    filterName="style"
                    filterTitle="Style"
                    filter={this.props.filters.style}
                    setFilter={this.props.setMultipleFilter}
                    clearFilter={this.props.clearOneFilter}
                />

                <Button label="Clear all filters" onClick={this.props.clearAllFilters}/>
            </div>
        );
    }
}
