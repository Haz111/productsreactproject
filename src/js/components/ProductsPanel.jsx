import React from "react";
import ProductsGrid from "./ProductsGrid.jsx";
import UsedFilters from "./UsedFilters.jsx";
import GridTypeSelector from "./GridTypeSelector.jsx";

export default class ProductsPanel extends React.Component {

    render() {
        let cssClasses = "ProductsPanel";
        return (
            <div className={cssClasses}>
                <div className="leftLine">
                    <div className="innerLeftLine">
                        <div className="productsHead">
                            <span className="searchResults">Search Results: {this.props.products.length}</span>
                            <select className="listedAmountSelection">
                                <option value="20">20</option>
                                <option value="40">40</option>
                                <option value="80">80</option>
                                <option value="All">All</option>
                            </select>
                            <GridTypeSelector />
                        </div>
                        <UsedFilters
                            filters={this.props.filters}
                            removeFilter={this.props.removeFilter}
                        />
                        <ProductsGrid
                            products={this.props.products}
                            toggleFavourite={this.props.toggleFavourite}
                        />
                    </div>
                </div>
                <p>Paginacja</p>
            </div>
        );
    }
}
