import React from "react";
import FilterCard from "./FilterCard.jsx";

export default class UsedFilters extends React.Component {

    constructor(props) {
        super(props);
        this.getCardsDetails = this.getCardsDetails.bind(this);
    }

    getCardsDetails() {
        let details = [];

        for (let filter in this.props.filters) {
            if (this.props.filters[filter].type === "multiple") {
                for (var i = 0; i < this.props.filters[filter].options.length; i++) {
                    if (this.props.filters[filter].options[i].checked) {
                        let detail = {
                            filterName: filter,
                            optionIndex: i,
                            cardName: this.props.filters[filter].options[i].name
                        };
                        details.push(detail);
                    }
                }
            }
        }

        return details;
    }

    render() {
        let cssClasses = "UsedFilters";

        let cardsOptions = this.getCardsDetails();
        return (
            <div className={cssClasses}>
                <strong>Showing filters:</strong>
                {cardsOptions.map((card, i) =>
                    <FilterCard
                        key={i}
                        cardDetails={card}
                        removeFilter={this.props.removeFilter}
                    />
                )}
            </div>
        );
    }
}
