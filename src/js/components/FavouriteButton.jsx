import React from "react";

export default class FavouriteButton extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let cssClasses = "FavouriteButton" ;

        return (
            <img
                className={cssClasses}
                onClick={() => {this.props.onClick()}}
                src={this.props.isFavourite ? "/images/heart_red.png" : "/images/heart_gray.png"}
            />
        );
    }
}

