import React from "react";

export default class FilterCard extends React.Component {

    constructor(props) {
        super(props);

        this.removeThisFilter = this.removeThisFilter.bind(this);
    }

    removeThisFilter() {
        this.props.removeFilter(this.props.cardDetails.filterName, this.props.cardDetails.optionIndex)
    }

    render() {
        let cssClasses = "FilterCard";

        return (
            <span className={cssClasses}>
                <span>{this.props.cardDetails.cardName}</span><span onClick={this.removeThisFilter}>x</span>
            </span>
        );
    }
}
